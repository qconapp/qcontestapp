package com.example.jlemmnitz.qcontestapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainBrowser extends ActionBarActivity  implements View.OnClickListener{


    Button btnOpenBrowser, btnExit;
    TextView eTInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_browser);

        btnOpenBrowser = (Button) findViewById(R.id.btnOpenBrowser);
        btnOpenBrowser.setOnClickListener(this);
        btnExit = (Button) findViewById(R.id.btnExit);
        btnExit.setOnClickListener(this);
        Toast.makeText(this, "Start Done", Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_browser, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view)
    {

        if ( view == findViewById(R.id.btnOpenBrowser))
        {
            Toast.makeText(this, "Browser wird geöffnet:", Toast.LENGTH_SHORT).show();
            Intent intBrws = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.q-con.eu"));
            startActivity(intBrws);
        }
        else if ( view == findViewById(R.id.btnExit))
        {
            Toast.makeText(this, "App wird beendet.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
